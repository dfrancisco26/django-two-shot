from django.contrib import admin
from receipts.models import Receipt, Account, ExpenseCategory
# Register your models here.
@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    receipt_display = ("vendor", "total", "date", "purchaser", "category")

@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    account_display = ("name", "owner")

@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    expense_display = ("name", "owner")